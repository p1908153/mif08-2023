<!-- LTeX: language=fr -->
# Nouvelles du cours

Les nouvelles du cours apparaîtront ici au fur et à mesure.

## 22/2/2023 : typo TP1, TP2 en ligne

J'avais pourtant bien fait la modification, mais j'ai glissé au moment de la
mettre en ligne :-(. L'énoncé du TP1 vous donnait la mauvaise URL à cloner (avec
2021 dans l'URL, alors que nous sommes bien en 2023). Vous pouvez vérifier que
vous avez bien le bon dépôt avec :

```
$ git remote -v
origin  https://forge.univ-lyon1.fr/matthieu.moy/mif08-2023 (fetch)
origin  https://forge.univ-lyon1.fr/matthieu.moy/mif08-2023 (push)
```

Le TP2 est en ligne. Vous aurez une séance dédiée le 16/3, mais le délai pour le
rendu sera assez court et la deadline stricte, donc je vous recommande de
prendre un peu d'avance.
